<?php

//SOMETHING IN THIS WILL WHITE SCREEN A SITE. I WILL TRACK DOWN AND FIX

//testing modifications to file in Bb - and trying again, more mods

/******************************************************************************************
*
* WP functions 
*
*******************************************************************************************/


/******************************************
Admin Client Branding Customizations
*******************************************/


// Custom WordPress Login Logo
function swp_custom_login_logo()
{
    echo '<style  type="text/css"> h1 a {  background-image:url(' . get_bloginfo('template_directory') . '/images/swp.png)  !important; } </style>';
}
add_action('login_head',  'swp_custom_login_logo');

function my_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="/path/to/style.css" />';
}
add_action('login_head', 'my_login_css');


/**
 * function my_login_imgurl
 * Set the URL to which the login logo image is linked
 *
 * @since 1.0
 */
function my_login_imgurl() {
	return home_url();
}
add_filter( 'login_headerurl', 'my_login_imgurl');


/**
 * function my_login_imgtitle
 * Set the title of the login page
 *
 * @since 1.0
 */
function my_login_imgtitle() {
	return get_bloginfo('title', 'display');
}
add_filter( 'login_headertitle', 'my_login_imgtitle');


/******************************************
Admin SWP Branding & Support customizations
*******************************************/


// Customize WP Admin Footer
function remove_footer_admin () {
    echo '&copy; 2012 - Made by <a target="_blank" href="http://switchwp.com">SwitchWP</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');


// Add a widget in WP Dashboard
function swp_dashboard_widget_function() {
	echo "<ul>
	<li>Put some text</li>
	<li>here</li>
	<li>and hre</li>
	<li>Hosting provider: ADD ONE</li>
	<li>DNS Host: ADD ONE</li>
	</ul>";
}
function swp_add_dashboard_widgets() {
	wp_add_dashboard_widget('wp_dashboard_widget', 'Technical information', 'swp_dashboard_widget_function');
}

add_action('wp_dashboard_setup', 'swp_add_dashboard_widgets' );


/******************************************
Admin General cleanup and mods
*******************************************/

/**
 * function my_alert
 * Add a custom alert to the WordPress admin panel
 *
 * @since 1.0
 */
function my_alert() {
	echo '<div class="error">';
	echo '<p>You are a shithead.</p>';
	echo '</div>';
}
add_action('admin_notices', 'my_alert');


/**
 * function my_blog_rss_output
 * Build an RSS feed widget
 *
 * @since 1.0
 */
function my_blog_rss_output() {
	echo '<div class="rss-widget">';

	wp_widget_rss_output( array(
		'url'          => 'http://www.website.com/path/to/feed/',
		'title'        => 'Widget Title',
		'items'        => 2, // Number of items to display
		'show_summary' => 1, // Boolean: show article excerpt
		'show_author'  => 1, // Boolean: show article author
		'show_date'    => 1, // Boolean: show article date
	) );

	echo "</div>";
}

/**
 * function my_blog_rss_widget
 * Display the RSS widget you built above
 *
 * @since 1.0
 */
function my_blog_rss_widget() {
	add_meta_box( 'my-blog-rss', 'Widget Title', 'my_blog_rss_output', 'dashboard', 'side', 'high' );
}
add_action('wp_dashboard_setup', 'my_blog_rss_widget');


// Remove dashboard widgets
add_action('wp_dashboard_setup', 'swp_dashboard_widgets');
function swp_dashboard_widgets() {
	global $wp_meta_boxes;
	// Today widget
	//unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	// Last comments
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	// Incoming links
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	// Plugins
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	// WP blog
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	// Other WP news
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}


// Remove meta boxes from posts, also good for pages or CPT 
   function remove_default_post_screen_metaboxes() {
 remove_meta_box( 'postcustom','post','normal' ); // Custom Fields Metabox
 remove_meta_box( 'postexcerpt','post','normal' ); // Excerpt Metabox
 remove_meta_box( 'commentstatusdiv','post','normal' ); // Comments Metabox
 remove_meta_box( 'trackbacksdiv','post','normal' ); // Talkback Metabox
 remove_meta_box( 'slugdiv','post','normal' ); // Slug Metabox
 remove_meta_box( 'authordiv','post','normal' ); // Author Metabox
}
   add_action('admin_menu','remove_default_post_screen_metaboxes');
   

/******************************************
Formatting an basic funcitonality
*******************************************/


// Custom image sizes
add_image_size( 'Slideshow', 520, 280, TRUE );


// Post Formats
add_theme_support( 'post-formats', array( 'aside', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video', 'audio' ));


/******************************************
TinyMCE mods
*******************************************/

 
// Put TinyMCE CSS styles in theme style.css for customizing look
add_editor_style('style.css');


// Add TinyMCE button that WP has removed - for all see http://www.tinymce.com/wiki.php/Buttons/controls
function swp_mce_buttons_2($buttons) {
	$buttons[] = 'hr';
	
	return $buttons;
}

add_filter('mce_buttons_2', 'swp_mce_buttons_2');


// Shows the 'kitchen sink' always
$format_array['wordpress_adv_hidden'] = false;


// hides the colour options and the kitchen sink
$format_array['theme_advanced_disable'] = 'forecolor, wp_adv';


// Modify WHOLE TinyMCE
function make_mce_awesome( $init ) {
	// Allow these block elements
    $init['theme_advanced_blockformats'] = 'h3,h4,h5,h6,p';
    // Turn off spell checker
    $init['theme_advanced_disable'] = 'underline,spellchecker,wp_help';
    // Text colours, how to turn off completely
    $init['theme_advanced_text_colors'] = '0f3156,636466,0486d3';
    // Add p class wrap!!!!!!!!!!!!
    $init['theme_advanced_buttons2_add'] = 'styleselect';
    $init['theme_advanced_styles'] = "bigTitle=bigTitle;Call To Action Button=ctaButton,Rounded Corners=rounded";
    
    return $init;
}

 
add_filter('tiny_mce_before_init', 'make_mce_awesome');


// Add Thumbnails in Manage Posts/Pages List
if ( !function_exists('AddThumbColumn') && function_exists('add_theme_support') ) {

    // for post and page
    add_theme_support('post-thumbnails', array( 'post', 'page' ) );

    function AddThumbColumn($cols) {

        $cols['thumbnail'] = __('Thumbnail');

        return $cols;
    }

    function AddThumbValue($column_name, $post_id) {

            $width = (int) 35;
            $height = (int) 35;

            if ( 'thumbnail' == $column_name ) {
                // thumbnail of WP 2.9
                $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
                // image from gallery
                $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
                if ($thumbnail_id)
                    $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
                elseif ($attachments) {
                    foreach ( $attachments as $attachment_id => $attachment ) {
                        $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
                    }
                }
                    if ( isset($thumb) && $thumb ) {
                        echo $thumb;
                    } else {
                        echo __('None');
                    }
            }
    }

    // for posts
    add_filter( 'manage_posts_columns', 'AddThumbColumn' );
    add_action( 'manage_posts_custom_column', 'AddThumbValue', 10, 2 );

    // for pages
    add_filter( 'manage_pages_columns', 'AddThumbColumn' );
    add_action( 'manage_pages_custom_column', 'AddThumbValue', 10, 2 );
}

// unregister all default WP Widgets
function unregister_default_wp_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
}

add_action('widgets_init', 'unregister_default_wp_widgets', 1);

// Remove menu items from the Admin Bar
function dashboard_tweaks() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
}
add_action( 'wp_before_admin_bar_render', 'dashboard_tweaks' );


// Add a link to the WP Toolbar
function custom_toolbar_link($wp_admin_bar) {
	$args = array(
		'id' => 'swp',
		'title' => 'SwitchWP', 
		'href' => 'http://switchwp.com', 
		'meta' => array(
			'class' => 'swp', 
			'title' => 'whatishtis'
			)
	);
	$wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'custom_toolbar_link', 999);


/******************************************
Security and Performance
*******************************************/

// Remove WP Jquery...
add_action( 'init', 'jquery_register' );


// ...and add it back from Google
function jquery_register() {

if ( !is_admin() ) {

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ( 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js' ), false, null, true );
    wp_enqueue_script( 'jquery' );
}
}


// Remove version info from head and feeds
function complete_version_removal() {
    return '';
}

add_filter('the_generator', 'complete_version_removal');


/**
 *
 * This is where we'll add more code
 *
 */
